//
//  ViewController.swift
//  audio-sample
//
//  Created by 多和田侑 on 2015/11/29.
//  Copyright © 2015年 Yu Tawata. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction
    func onStartRecordingClick(sender: AnyObject) {
        Reader.sharedInstance.startRecording()
    }

    @IBAction
    func onStopRecordingClick(sender: AnyObject) {
        Reader.sharedInstance.stopRecording()
    }

}

