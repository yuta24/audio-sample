//
//  Reader.swift
//  audio-sample
//
//  Created by 多和田侑 on 2015/11/29.
//  Copyright © 2015年 Yu Tawata. All rights reserved.
//

import Foundation
import AVFoundation

public protocol ReaderDelegate {
    func nofity()
}

public class Reader {

    private class UserData {
        private var reader: Reader?
    }

    static let sharedInstance = Reader()

    class func requestPermission() {
        guard AVAudioSession.sharedInstance().respondsToSelector("requestRecordPermission:") else {
            return
        }

        AVAudioSession.sharedInstance().requestRecordPermission({isAllow in
            guard isAllow else {
                sharedInstance.isAllow = false
                // TODO: マイク許可なしを通知
                return
            }

            sharedInstance.isAllow = true
        })
    }

    private let numBuffers = 3

    private var isAllow: Bool
    private var isRecording: Bool

    // AIFF 16bit 44.1kHz STEREO
    private var dataFormat: AudioStreamBasicDescription
    private var inputCallback: AudioQueueInputCallback

    private var queue: AudioQueueRef
    private var buffers: [AudioQueueBufferRef]

    private var userData: UserData

    public var delegate: ReaderDelegate?

    private init() {
        isAllow     = false
        isRecording = false

        dataFormat = AudioStreamBasicDescription(
            mSampleRate:        44100.0,
            mFormatID:          kAudioFormatLinearPCM,
            mFormatFlags:       kAudioFormatFlagIsBigEndian
                | kAudioFormatFlagIsSignedInteger
                | kAudioFormatFlagIsPacked,
            mBytesPerPacket:    4,
            mFramesPerPacket:   1,
            mBytesPerFrame:     4,
            mChannelsPerFrame:  2,
            mBitsPerChannel:    16,
            mReserved:          0
        )
        inputCallback = {
            (inUserData, inAQ, inBuffer, inStartTime, inNumberPacketDescriptions, inPacketDescs) in

            print("debug")

            let userData = UnsafePointer<UserData>(inUserData).memory

            guard (userData.reader != nil) else {
                return
            }

            // FIXME: バッファデータ取り出し部は怪しい。。。
            let buffer = inBuffer.memory
            let bufferSize = buffer.mAudioDataByteSize / UInt32(sizeof(Int16))
            let rawData = Array(UnsafeBufferPointer<Int16>(start: UnsafePointer<Int16>(buffer.mAudioData), count: Int(bufferSize)))

            print(rawData)

            AudioQueueEnqueueBuffer(userData.reader!.queue, inBuffer, 0, nil)
            // TODO: 実装
//            let rec:Recorder = UnsafeMutablePointer<Recorder>(inUserData).memory
//            if inNumberPacketDescriptions > 0 {
//                rec.analyze(inBuffer)
//            }
        }

        queue   = AudioQueueRef()
        buffers = Array<AudioQueueBufferRef>(count: numBuffers, repeatedValue: AudioQueueBufferRef())

        userData = UserData()
        userData.reader = self
    }

    private func setup() {
        AudioQueueNewInput(&dataFormat, inputCallback, &userData, CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &queue)

        for var buffer in buffers {
            AudioQueueAllocateBuffer(queue, UInt32(dataFormat.mSampleRate/10.0) * dataFormat.mBytesPerFrame, &buffer)
            AudioQueueEnqueueBuffer(queue, buffer, 0, nil)
        }
    }

    func startRecording() {
        guard isAllow else {
            self.dynamicType.requestPermission()
            return
        }

        guard !isRecording else {
            return
        }

        setup()
        isRecording = true
        AudioQueueStart(queue, nil)
    }

    func stopRecording() {
        isRecording = false

        AudioQueueFlush(queue)
        AudioQueueStop(queue, false)

        for buffer in buffers {
            AudioQueueFreeBuffer(queue, buffer)
        }

        AudioQueueDispose(queue, true)
    }

}
